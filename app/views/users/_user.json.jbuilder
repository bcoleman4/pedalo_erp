json.extract! user, :id, :firstname, :surname, :email, :password, :created_at, :updated_at
json.url user_url(user, format: :json)
