source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'


# ========================================================
# Gems added by BC

# Use for stylised charts
gem 'lazy_high_charts'
# Used for secure login/authentication 
gem 'devise'
# Use for access control
gem 'cancancan'
# Use for error handling
gem 'better_errors'
# Use for scheduled jobs
gem 'delayed_job'
# Use to deal with attachments
gem 'paperclip'
# Use for testing
gem 'rspec-rails'
# Use to create test data
gem 'factory_girl_rails'
# Use for debugging
gem 'pry-rails'
# Use for search
gem 'ransack'
# Use for security analysis
gem 'brakeman'
# Use for background processing
gem 'sidekiq'
# Use for database cleaning
gem 'cucumber'
# Use for CMS
gem 'refinerycms'
# Use for multi customer set-up
gem 'acts_as_tenant'
# Use for js frontend
gem 'react-rails'
# Use for test coverage
gem 'simplecov'
# Use for secure headers
#gem 'secure_headers'
# Admin functions
gem 'activeadmin'
# Use for PDF generation
gem 'prawn'
# Use for datepicker
gem 'bootstrap-datepicker-rails' 
# Use of bootstrap for styling
gem 'bootstrap'
# Use of bootstrap for styling
gem 'bootstrap-sass'
# Use for JQuery
gem 'jquery-rails'
# Use to enable REPL and local variable inspection
gem 'binding_of_caller'
# Use for drag and drop
gem 'jquery-ui-rails'
gem 'rails_sortable'
# Use for email
gem 'mail'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
