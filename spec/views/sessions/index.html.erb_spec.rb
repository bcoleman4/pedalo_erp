require 'spec_helper'

RSpec.describe "sessions/index", type: :view do
  before(:each) do
    assign(:sessions, [
      Session.create!(
        :email => "Email",
        :password => "Password"
      ),
      Session.create!(
        :email => "Email",
        :password => "Password"
      )
    ])
  end

  it "renders a list of sessions" do
    render
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Password".to_s, :count => 2
  end
end
